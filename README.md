# CI1237 - Matemática Discreta

**Enunciado adaptado das Notas de aula, disponibilizadas durante o ensino remoto emergencial,  primeiro semestre de 2021**

## Conteúdo

### Parte I - Indução
- Elementos de lógica
- Conjuntos e inteiros
- Aproximação assintótica
- Piso e teto
- Indução
- Descrições Recursivas
- Funções Iteradas

### Parte II - Recorrências
- Recorrências Iteradas
- Recorrências lineares
    -  Homogêneas
    - Não homogêneas
- Somatórios

### Parte III - Contagem
- Fundamentos de contagem
- União e produto cartesiano
- Sequências
- Funções
- Subconjuntos
- Composições
- Inclusão/Exclusão
- Pontos fixos de permutações e desarranjos
- Funções sobrejetoras e partições

