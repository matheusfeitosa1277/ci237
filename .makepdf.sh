#!/bin/bash
#Recebe como arg um path para um dir
#Criar arquvios pdf a partir dos arquivos .tex
#Move os mesmos para o dir pdf

pushd "$1"; # dir dos arquivos .tex
mkdir pdf;

for file in ./*.tex; do
    pdflatex $file;
    pdflatex $file;
done 

rm -f *.log *.aux;
mv *.pdf ./pdf;

popd;
