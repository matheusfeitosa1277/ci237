#include <stdio.h>

int a() {
    int i, t = 0;

    for (i = 1; i <= 100; i++)
        if (!(i%3)) {
            printf("%d ", i);
            ++t;
        }

    putchar('\n');

    printf("%d\n", t);

    return t;
}

int b() {
    int i, t = 0;

    for (i = 1; i <= 500; i++)
        if (!(i%4)) {
            printf("%d ", i);
            ++t;
        }

    putchar('\n');

    printf("%d\n", t);

    return t;
}

int h(int n, int k) {
    return k/n;
}

int main() {

    a(); b();

    printf("%d %d\n", h(3, 100), h(4, 500));

    return 0;
}

