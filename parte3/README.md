Tudo aqui e um rascunho, nada foi revisado...

### Parte III - Contagem
- Fundamentos de contagem
- União e produto cartesiano
- Sequências
- Funções
- Subconjuntos
- Composições
- Inclusão/Exclusão
- Pontos fixos de permutações e desarranjos
- Funções sobrejetoras e partições
