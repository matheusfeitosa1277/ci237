#include <stdio.h>

#define NUM 1000

int main() {
    int i, count;

    for (i = 1, count = 0; i <= NUM; ++i) 
        if ((i&1) == 1 || i*i <= NUM) 
            ++count;
    
    printf("%d\n", count);
}

