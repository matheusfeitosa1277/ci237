#include <stdio.h>

int main() {
    int i, count = 0;

    for (i = 1; i <= 2001; i++) 
        if ( (i%3 == 0 || i%4 == 0) && i%5 != 0)
            ++count;

    printf("%d\n", count);

    return 0;
}

