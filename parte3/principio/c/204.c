#include <stdio.h>

int main() {
    int i, count = 0;

    for (i = 1; i <= 1000; i++) 
        if (i%3 == 0 || i%5 == 0 || i%7 == 0)
            ++count;

    printf("%d\n", count);

    return 0;
}

