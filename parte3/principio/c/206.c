#include <stdio.h>

int main() {
    int i, count = 0;

    for (i = 1; i <= 10000; i++) 
        if (i%4 == 0 || i%6 == 0 || i%10 == 0)
            ++count;

    printf("%d\n", count);

    return 0;
}

