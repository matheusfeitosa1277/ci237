#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long summatory(unsigned int n) {
    int i, sum = 0; 

    for (i = 0; i <= n; i++) 
        sum += iPow(i, 2);
    

    return sum;
}

long long s(unsigned int n) {
    return n*(2*iPow(n, 2) + 3*n + 1)/6;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("Summatory(n) = %lld, s(n) = %lld\n", summatory(i), s(i));
    
    return 0;
}

