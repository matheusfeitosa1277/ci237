#include <stdio.h>
#include "lib/math.h"

//f:N->R

long double summatory(unsigned int n) {
    int i;
    long double sum = 0, base = 1;

    for (i = 0; i <= n; i++) {
        sum += iPow(i, 2)/base;
        base *= 5;
    }

    return sum;
}

long double s(unsigned int n) {
    return (-(8*iPow(n, 2) + 20*n + 15)/(long double)iPow(5, n) + 15)/32;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("Summatory(n) = %LF, s(n) = %LF\n", summatory(i), s(i));
    
    return 0;
}

