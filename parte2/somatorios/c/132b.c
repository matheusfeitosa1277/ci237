#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long summatory(unsigned int n) {
    int i, sum = 0;

    for (i = 0; i <= n; i++)
        sum += i*(i - 1);

    return sum;
}

long long s(unsigned int n) {
    return (iPow(n, 3) - n)/3;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("Summatory(n) = %lld, s(n) = %lld\n", summatory(i), s(i));
    
    return 0;
}

