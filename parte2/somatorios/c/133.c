#include <stdio.h>
#include <math.h>

double g(int n) {
    double aux = 0;

    for (int i = 1; i <= log2(n); ++i)
        aux += pow(2, i-1);
    
    return (floor(log2(n))+1)*((n-aux)/n);
}

double f(int n) {
    double aux = 0;

    for (int i = 1; i <= log2(n); ++i)
        aux += i*pow(2, i-1);

    return aux/n;
}

double u(int n) { 
    return f(n) + g(n);
}

double s(int n) {
    int F_LOG2 = floor(log2(n));

    return (-pow(2, F_LOG2+1)+F_LOG2+n+2)/n + F_LOG2;
}

int main() {
    double aux;

    for (int n = 1; ; ++n) {
        aux = s(n);
        printf("%4d | %lf | %lf | %lf\n", n, u(n), aux, aux/floor(log2(n)));
    }

    return 0;
}

