#include <stdio.h>

//f:N->N

long long summatory(unsigned int n) {
    int i, sum = 0;

    for (i = 0; i <= n; i++)
        sum += i;

    return sum;
}

long long s(unsigned int n) {
    return n*(n + 1)/2;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("Summatory(n) = %lld, s(n) = %lld\n", summatory(i), s(i));
    
    return 0;
}

