#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long summatory(unsigned int n) {
    int i, sum = 0, base = 1;

    for (i = 0; i <= n; i++) {
        sum += iPow(i, 2)*base;
        base *= 3;
    }

    return sum;
}

long long s(unsigned int n) {
    return (iPow(3, n + 1)*(iPow(n, 2) - n + 1) - 3)/2;
}

int main() {
    int i;

    for (i = 0; i <= 14; i++) //Overflow muito rapido
        printf("Summatory(n) = %lld, s(n) = %lld\n", summatory(i), s(i));
    
    return 0;
}

