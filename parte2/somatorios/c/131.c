#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long summatory(unsigned int n) {
    int i, sum = 0, base = 1;

    for (i = 0; i <= n; i++) {
        sum += i*base;
        base *= 2;
    }

    return sum;
}

long long s(int n) {
    return iPow(2, n + 1)*(n - 1) + 2;
}

int main() {
    int i;

    for (i = 0; i <= 20; i++)
        printf("Summatory(n) = %lld, s(n) = %lld\n", summatory(i), s(i));
    
    return 0;
}

