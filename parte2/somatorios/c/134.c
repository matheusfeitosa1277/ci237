#include <stdio.h>
#include <math.h>

//f:N->N

double SQRT_5 = sqrt(5.0);

long long fib(int n) {
    if (n <= 1)
        return n;
    return fib(n-1) + fib(n-2);
}

long long f(int n) {
    int i, sum = 0;

    for (i = 0; i <= n; i++)
        sum += fib(i);

    return sum;
}

long long s(int n) {
    return pow((1-SQRT_5)/2, n)*((5-3*SQRT_5)/10) + pow((1+SQRT_5)/2, n)*((5+3*SQRT_5)/10)- 1;
}

int main() {
    int i;

    for (i = 0; i <= 21; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

