#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long summatory(int n) {
    int i, sum = 0;

    for (i = 0; i <= n; i++) 
        sum += iPow(i, 2)*(i - 1);

    return sum;
}

long long s(unsigned int n) {
    int aux = iPow(n, 2);

    return (3*aux + 2*n)*(aux - 1)/12;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("Summatory(n) = %lld, s(n) = %lld\n", summatory(i), s(i));
    
    return 0;
}

