#include <stdio.h>
#include <math.h>
#include "lib/math.h"

#define F_0 0
#define F_1 0

#define LOG3_N2 floorl((log2l(n) - 1)/log2l(3))

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;

    return 4*f(n/3) + iPow(n, 2) - 7*n + 5;
}

long long hUn(unsigned int n) {
    return n/powl(3, LOG3_N2 + 1);
}

long long summatory(unsigned int n, int u) {
    int i, aux, sum = 0, div = 1, base = 1;

    for (i = 0; i <= u; i++) {
        aux = n/div;
        sum += (iPow(aux, 2) - 7*aux + 5)*base;
        div *= 3; base *= 4;
    }

    return sum;
}

long long s(unsigned int n) {
    return f(hUn(n))*powl(4, LOG3_N2 + 1) + summatory(n, LOG3_N2);
}

int main() {
    int i;

    for (i = 2; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

