#include <stdio.h>
#include <math.h>

#define F_1 42 

//f:N->N

long long f(unsigned int n) {
    if (n == 1)
        return F_1;

    return 2*f(ceill(n/2)) + 1;
}


long long summatory(unsigned int n, int u) {
    int i, sum = 0, base = 1;

    for (i = 0; i <= u; i++) {
        sum += base;
        base *= 2;
    }

    return sum;
}

long long s(unsigned int n) {
    if (n == 1)
        return F_1;

    return F_1*powl(2, floorl(log2l(n))) + summatory(n, log2l(n) - 1);
}

int main() {
    int i;

    for (i = 2; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

