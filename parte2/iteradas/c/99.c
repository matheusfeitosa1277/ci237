#include <stdio.h>
#include "lib/math.h"

#define F_0 0
#define F_1 0

//f:N->N

long long f(unsigned int n) {
    if (n >= 2)
        return f(n-2) + 1;

    if (n == 1)
        return F_1;

    return F_0;
}

long long s(unsigned int n) {
    return f(n % 2) + n/2;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

