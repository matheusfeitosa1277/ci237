#include <stdio.h>
#include <math.h>

#define F_0 0
#define F_1 0

#define LG_6 log2l(6)
#define LOG6_N2 (log2l(n) - 1)/LG_6 // \log_6\PAR{\frac{n}{2}} = log_6(n/2)

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;

    return 6*f(n/6) + 2*n + 3;
}

long long hUn(unsigned int n) {
    return n/powl(6, floorl(LOG6_N2) + 1);
}

long long summatory(unsigned int n, int u) {
    int i, sum = 0, base = 1;

    for (i = 0; i <= u; i++) {
        sum += (2*(n/base) + 3)*base;
        base *= 6;
    }

    return sum;
}

long long s(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;

    return f(hUn(n))*powl(6, floorl(LOG6_N2) + 1) + summatory(n, LOG6_N2);
}

int main() {
    int i;

    for (i = 0; i <= 42; i++) 
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));


    return 0;
}

