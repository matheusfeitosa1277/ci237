#include <stdio.h>
#include <math.h>

//f:N^*->N

long long f(unsigned int n) {
    if (n == 1)
        return 1;

    return f(n/2) + 1;
}

long long s(unsigned int n) {
    return floorl(log2l(n)) + 1;
}

int main() {
    int i;

    for (i = 1; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

