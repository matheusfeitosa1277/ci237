#include <stdio.h>
#include <math.h>
#include "lib/math.h"

#include <stdio.h>

#define F_1 01 

//f:N->N

long long f(unsigned int n) {
    if (n == 1)
        return F_1;

    return 2*f(n/2) + 6*n - 1;
}


long long summatory(unsigned int n, int u) {
    int i, sum = 0, base = 1;

    for (i = 0; i <= u; i++) {
        sum +=  (6*(n/base) - 1)*base;
        base *= 2;
    }


    return sum;
}

long long s(unsigned int n) {
    int lg = log2l(n);

    return F_1*iPow(2, lg) + summatory(n, lg - 1);
}


int main() {
    int i;

    for (i = 1; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

