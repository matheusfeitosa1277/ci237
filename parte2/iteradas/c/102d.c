#include <stdio.h>
#include <math.h>
#include "lib/math.h"

#define F_0 0
#define F_1 0

#define LG_N floor(log2l(n))

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;

    return 3*f(n/2) + iPow(n, 2) - 2*n + 1;
}

long long hUn(unsigned int n) {
    return n/powl(2, log2l(n));
}

long long summatory(unsigned int n, int u) {
    int i, aux, sum = 0, div = 1, base = 1;

    for (i = 0; i <= u; i++) {
        aux = n/div;
        sum += (iPow(aux, 2) - 2*aux + 1)*base;
        div *= 2; base *= 3;
    }

    return sum;
}

long long s(unsigned int n) {
    return f(hUn(n))*powl(3, LG_N) + summatory(n, LG_N - 1);    
}

int main() {
    int i;

    for (i = 2; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

