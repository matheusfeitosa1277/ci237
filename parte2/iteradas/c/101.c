#include <stdio.h>
#include <math.h>
#include <stdio.h>

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return 0;

    return f(n/2) + n % 2;
}

long long s(unsigned int n) {
    int i, sum = 0, base = 1;

    if (n > 0) {
        for (i = 0; i <= (int)floorl(log2l(n)); i++) {
            sum += n/base % 2;
            base *= 2;
        }
    }

    return sum;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

