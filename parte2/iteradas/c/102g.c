#include <stdio.h>
#include <math.h>

#define F_0 0
#define F_1 0

#define LOG5_N2 floorl((log2l(n) - 1)/log2l(5))

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;

    return 2*f(n/5) + n - 1;
}

long long hUn(unsigned int n) {
    return n/powl(5, LOG5_N2 + 1);
}

long long summatory(unsigned int n, int u) {
    int i, sum = 0, div = 1, base = 1;

    for (i = 0; i <= u; i++) {
        sum += (n/div - 1)*base;
        div *= 5; base *= 2;
    }

    return sum;
}

long long s(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;
    
    return f(hUn(n))*powl(2, LOG5_N2 + 1) + summatory(n, LOG5_N2);
}

int main() {
    int i;

    for (i = 2; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

