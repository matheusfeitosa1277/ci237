#include <stdio.h>
#include <math.h>

#define F_0 0
#define F_1 0
#define F_2 0

//log_{3/2}(n/2) = (lg n - 1)/lg (3/2)
#define LOG32_N3 floorl((log2l(n) - log2l(3))/log2l(1.5))
#define K 1 

//f:N->N

long long f(long double n) {

    if (n < 3)
        switch ((int)n) {
            case 0: return F_0;
            case 1: return F_1;
            case 2: return F_2;
        }

    return f(ceill(2*n/3)) + K;
}

long long hUn(long double n) {
    return ceill(powl(2.0/3.0, LOG32_N3 + 1)*n);
}


long long s(unsigned int n) {
    if (n == 0)
        return F_0;

    if (n == 1)
        return F_1;

    return f(hUn(n)) + (LOG32_N3 + 1)*K;
}

int main() {
    int i;

    for (i = 2; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

