#include <stdio.h>
#include <math.h>
#include "lib/math.h"

#define F_0 0
#define F_1 0
#define F_2 0
#define F_3 0

#define LOG3_N3 floorl((log2l(n) - 2)/log2l(3))

//f:N->N

long long f(unsigned int n) {

    if (n < 4) 
        switch (n) {
            case 0: return F_0; break;
            case 1: return F_1; break;
            case 2: return F_2; break;
            case 3: return F_3; break;
        }

    return 4*f(n/3) + (int)sqrtl(n) + 1;

}

long long hUn(unsigned int n) {
    return n/powl(3, LOG3_N3 + 1);
}

long long summatory(unsigned int n, int u) {
    int i, aux, sum = 0, div = 1, base = 1;

    for (i = 0; i <= u; i++) {
        aux = floorl(sqrtl(n/iPow(3, i))); //...
        sum += (aux + 1)*base;
        div *= 3; base *= 4;
    }

    return sum;
}

long long s(unsigned int n) {
    return f(hUn(n))*powl(4, LOG3_N3 + 1) + summatory(n, LOG3_N3);
}

int main() {
    int i;

    for (i = 4; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

