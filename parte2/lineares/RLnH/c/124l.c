#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n < 2)
        return n;

    return 5*f(n-1) - 4*f(n-2) + 2*n*iPow(5, n) + 2;
}

long long s(unsigned int n) {
    return (12*n*(3*iPow(5, n+2) - 4) - 153*iPow(5, n+2) + 455*iPow(2, 2*n + 3) + 185)/72;
}

int main() {
    int i;

    for (i = 0; i <= 21; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

