#include <stdio.h>
#include <math.h>

int p(int n) {
    if (!n)
        return 1;

    return 2*p(n-1);
}

int l(int n) {
    if (!n)
        return 0;

     return 2*l(n-1) + p(n-1);
}

int l_(int n) {
    return n*pow(2, n-1);
}

int main() {
    for (int n = 0; n < 10; ++n)
        printf("%d %d\n", p(n), l_(n));

    return 0;
}
