#include <stdio.h>

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return 0;

    return f(n-1) + n;
}

long long s(unsigned int n) {
    return n*(1+n)/2;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

