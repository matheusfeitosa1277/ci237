#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n < 2)
        return n;

    return 7*f(n-1) - 12*f(n-2) + 2*n;
}

long long s(unsigned int n) {
    return (23*iPow(2, 2*n + 1) - 7*iPow(3, n + 2) + 6*n + 17)/18;
}

int main() {
    int i;

    for (i = 0; i <= 21; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

