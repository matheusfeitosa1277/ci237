#include <stdio.h>
#include "lib/math.h"

#define F_0 0

//f:N->N

long long f(unsigned int n) {
    if (n < 1)
        return F_0;
    
    return 2*f(n-1) + iPow(n, 2);
}

long long s(unsigned int n) {
    return (F_0 + 6)*iPow(2, n) - iPow(n, 2) - 4*n - 6;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

