#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n < 1)
        return n;

    return 2*f(n-1) + iPow(n, 2) + n;
}

long long s(unsigned int n) {
    return 8*(iPow(2, n) - 1) - n*(5 + n);
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

