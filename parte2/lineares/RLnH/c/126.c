#include <stdio.h>
#include <math.h>

//f:N->N

double SQRT_5 = sqrt(5.0);

long long s(int n) {
    if (n <= 1)
        return 0;
    return s(n-1) + s(n-2) + 1;
}

long long f(int n) {
    return pow((1-SQRT_5)/2, n)*((5-SQRT_5)/10) + pow((1+SQRT_5)/2, n)*((5+SQRT_5)/10)- 1;
}

int main() {
    int i;

    for (i = 0; i <= 21; i++)
        printf("f(n) = %lld, s(n) = %lld\n", s(i), f(i));

    return 0;
}

