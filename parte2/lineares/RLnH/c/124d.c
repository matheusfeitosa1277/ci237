#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n < 1)
        return n;

    return 2*f(n-1) + n;
}

long long s(unsigned int n) {
    return iPow(2, n+1) - n - 2;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

