#include <stdio.h>

int p(int i, int j) {
    return (j*(j+1))/2 + i;
}

int main() {

    for (int j = 0; ; ++j) {
        for (int i = 0; i <= j; ++i)
            printf("%3d ", p(i,j));
        putchar('\n');
    }

    return 0;
}
