#include <stdio.h>
#include <math.h>

#define M_SQRT5 sqrtl(5)

//f:N->N

long long f(unsigned int n) {
    if (n < 2)
        return n;

    return f(n-1) + f(n-2) + 1;
}

long double s(unsigned int n) { //Problemas IEEE 754
    return  powl((1 + M_SQRT5)/2, n)*(3*M_SQRT5 + 5)/10 +
            powl((1 - M_SQRT5)/2, n)*(-3*M_SQRT5 + 5)/10 - 1;
}

int main() {
    int i;

    for (i = 0; i <= 42; i++)
        printf("f(n) = %lld, s(n) = %1.Lf\n", f(i), s(i));

    return 0;
}

