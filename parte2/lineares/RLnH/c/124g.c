#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n < 1)
        return n;

    return 2*f(n-1) + 2*n - 1;
}

long long s(unsigned int n) {
    return 3*(iPow(2, n) - 1) - 2*n;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));

    return 0;
}

