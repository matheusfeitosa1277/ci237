#include <stdio.h>
#include <math.h>
#include "lib/math.h"

//It just works

//f:N->R

// \frac{8\sqrt{2}^n - 2^{-n}}{7} <- Par
// \frac{15\sqrt{2}^{\frac{n-3}{2}} - 2^{-n}}{7} <- Impar

long double f(unsigned int n) {
    if (n <= 1)
        return 1;

    return 2*f(n-2) + 1.0/iPow(2, n);
}

long double s(int n) {
    long double aux =  - powl(2, -n);

    if ((n & 1) == 0) //even
        return (8*powl(M_SQRT2, n) + aux)/7;

    return (15*powl(2, (n - 3)/2.0) + aux)/7;
}

int main() {
    /*
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %Lf, s(n) = %Lf\n", f(n), s(n));
    */

    for (int i = 0; i <= 42; i++)
        printf("f(n) = %Lf, s(n) = %Lf\n", f(i), s(i));

    return 0;
}

