#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n < 2)
        return n;

    return 5*f(n-1) - 6*f(n-2) + n*iPow(3, n);
}

long long s(unsigned int n) {
    return 10*(iPow(3, n) - iPow(2, n)) + 3*n*(n*iPow(3, n) - iPow(3, n + 1))/2;
}

int main() {
    int i;

    for (i = 0; i <= 21; i++)
        printf("f(n) = %lld, s(n) = %lld\n", f(i), s(i));

    return 0;
}

