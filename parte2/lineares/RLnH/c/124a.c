#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n == 0)
        return 0;

    return f(n-1) + 1;
}

long long s(unsigned int n) {
    return n;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

