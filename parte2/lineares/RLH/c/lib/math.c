#include "math.h"

long long iPow(int b, int exp) {
    long long aux = 1;

    while (exp--) // exp != 0 ? --exp : break
        aux *= b;

    return aux;
}
