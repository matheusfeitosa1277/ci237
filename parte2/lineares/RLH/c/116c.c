#include <stdio.h>
#include "lib/math.h"

//f:N->Z

long long f(unsigned int n) {
    if (n < 1)
        return n;

    if (1 <= n && n <= 2)
        return 1;

    return 8*f(n-1) - 19*f(n-2) + 12*f(n-3);
}

long long s(unsigned int n) {
    return 2*iPow(3, n) - iPow(4, n) - 1;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

