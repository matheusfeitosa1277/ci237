#include <stdio.h>
#include "lib/math.h"

//f:N->Z

long long f(unsigned int n) {
    if (n <= 1)
        return n;

    if (n == 2)
        return 3;

    return 7*f(n-1) - 16*f(n-2) + 12*f(n-3);
}

long long s(unsigned int n) {
    return iPow(2, n)*(1 + n) - iPow(3, n);
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

