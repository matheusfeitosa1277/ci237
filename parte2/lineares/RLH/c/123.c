#include <stdio.h>

//f:N->N

long long f(unsigned int n) {
    if (n <= 4)
        return 1;
    
    return 7*f(n-1) - 19*f(n-2) + 25*f(n-3) - 16*f(n-4) + 4*f(n-5);
}

long long s(unsigned int n) {
    return 1;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

