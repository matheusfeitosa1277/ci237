#include <stdio.h>
#include "lib/math.h"

//f:N->C

long long f(unsigned int n) {
    if (n == 0)
        return 1;

    if (n == 1 || n == 2)
        return 9;

    return 9*f(n-1) - 27*f(n-2) + 27*f(n-3);
}

long long s(unsigned int n) {
    return - iPow(3, n)*(2*iPow(n, 2) - 4*n - 1);
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

