#include <stdio.h>
#include <math.h>

//Quick and dirty

//f:N->R

long double T_T = 2.0/3;

long double f(int n) {
    if (n <= 1)
        return n+1;

    return sqrtl(f(n-1)*f(n-2));
}

long double s(int n) {
    return powl(2, T_T*(1 - powl(-0.5, n)));
}

int main() {
    long double n;

    for (n = 0; n < 42; n++)
        printf("f(n) = %.20Lf, s(n) = %.20Lf\n", f(n), s(n));
}

