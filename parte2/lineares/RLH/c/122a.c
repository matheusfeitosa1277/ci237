#include <stdio.h>
#include <math.h>

//f:N->N

double SQRT_5 = sqrt(5.0);

long long f(unsigned int n) {
    if (n <= 1)
        return n;

    return n*f(n-1) + n*(n-1)*f(n-2);
}

long long s(double n) {
    long long g = (SQRT_5/5)*(pow((1+SQRT_5)/2, n) - pow((1-SQRT_5)/2, n));

    while(n)
        g *= n--;

    return g;
}

int main() {
    unsigned int n;

    for (n = 0; n < 10; n++)
        printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

