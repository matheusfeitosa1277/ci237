#include <stdio.h>

long long f(unsigned int n) {
    if (n <= 2)
        return n;

    return 5*f(n-1) - 7*f(n-2) + 3*f(n-3);
}

long long s(unsigned int n) {
    return n;
}

int main() {
    unsigned int n; 

    scanf("%u", &n);
    
    printf("f(n) = %lld,  s(n) = %lld\n", f(n), s(n));
}

