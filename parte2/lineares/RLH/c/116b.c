#include <stdio.h>
#include "lib/math.h"

//f:N->Z

long long f(unsigned int n) {
    if (n == 0)
        return 0;

    if (n == 1 || n == 2)
        return iPow(2, n);

    return  6*f(n-1) - 11*f(n-2) + 6*f(n-3);
}

long long s(unsigned int n) {
    return iPow(2, n+2) - iPow(3,n) - 3;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

