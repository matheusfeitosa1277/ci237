#include <stdio.h>

//f:N->N

long long iPow(int b, int exp) {
    long long aux = 1;

    while (exp--) // exp != 0 ? --exp : break
        aux *= b;

    return aux;
}

long long f(unsigned int n) {
    if (n <= 1)
        return n;

    return 5*f(n-1) - 6*f(n-2);
}

long long s(unsigned int n) {
    return iPow(3, n) - iPow(2, n);
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

