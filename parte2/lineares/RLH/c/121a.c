#include <stdio.h>
#include "lib/math.h"

//f:N->N

long long f(unsigned int n) {
    if (n <= 1)
        return 3;

    if (n == 2)
        return 7;

    return 3*f(n-1) - f(n-2) + 3*f(n-3);
}

long long s(unsigned int n) {
    int aux = 0;

    if ((n & 1) == 0) { //even
        if (n % 4 == 0)
            aux = 2;
        else aux = -2;
    }

    return iPow(3, n) + aux;
}

int main() {
    unsigned int n;

    scanf("%u", &n);

    printf("f(n) = %lld, s(n) = %lld\n", f(n), s(n));
}

