#include <stdio.h>
#include <math.h>

/* Definicao da Serie Harmonica */
double harmonica(int n){
   int i;
   double sum = 0; 

   for (i = 1; i <= n; i++)
       sum += 1.0/i;

   return sum;
}

/* Retorna ln(n)*/
//double log(double x);

int main(){
    int n;
    double Hn, ln;

    printf("Digite n: "); scanf("%d", &n);
    
    Hn = harmonica(n);
    ln = log(n);

    printf("n: %d\n", n);
    printf("Harmonica: %lf; ln: %lf; Euler-Mascheroni: %lf\n", Hn, ln, Hn - ln);
}

