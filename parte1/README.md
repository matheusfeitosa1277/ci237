# Tex 2

Para os proximos exercicios envolvendo prova por inducao eh aconselhavel usar os macros/template definidos em
/inducao/tex2 e ../macros.sty

Nao muda muito o formato final do pdf, mas fica muito mais facil de trabalhar em cima do .tex

### Parte I - Indução
- Elementos de lógica
- Conjuntos e inteiros
- Aproximação assintótica
- Piso e teto
- Indução
- Descrições Recursivas
- Funções Iteradas

