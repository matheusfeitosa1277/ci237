#include <stdio.h>
#include <complex.h>

/* Parte Esquerda do Predicado */
double complex summation(int n, double complex c){
    int i;
    double complex sum = CMPLX(0, 0);

    for (i = 0; i <= n; i++)
        sum += cpow(c, CMPLX(i, 0));

    return sum;
}

/* Parte Direita do Predicado */
double complex division(int n, double complex c){
    return (cpow(c, CMPLX(n, 0)+1) - 1)/(c-1);
}

int main(){
    int n;
    double real, imag;
    double complex sum, div, c;

    printf("Complex(Real, Imag), n: "); scanf("%lf %lf %d", &real, &imag, &n);

    c = real + imag * I;
    sum = summation(n, c);
    div = division(n, c);

    //Temos problemas de arredondamento
    printf("Sum - Real: %lf | Imag: %lf\n", creal(sum), cimag(sum));
    printf("Div - Real: %lf | Imag: %lf\n", creal(div), cimag(div));

    return 0;
}

