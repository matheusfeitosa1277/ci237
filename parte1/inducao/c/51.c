#include <stdio.h>
#include <math.h>

#define SQRT5 sqrt(5)

/* Definicao Recursiva de Fibonacci */
int F(int n){
    if (n <= 1) return n;

    return F(n-1) + F(n-2);
}

/* Recorrencia Resolvida */
int Fib(int n){
    return (SQRT5/5)*(pow((1+SQRT5)/2, n) - pow((1-SQRT5/2), n));
}

int main(){
    int n, rec, exp;

    printf("Digite n: "); scanf("%d", &n);
    
    rec = F(n);
    exp = Fib(n);

    printf("Rec:%d Exp:%d\n", rec, exp);
    
    return 0;
}

