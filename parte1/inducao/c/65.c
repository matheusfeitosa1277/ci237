#include <stdio.h>
#include <math.h>

int f(int n) {
    if (n < 2)
        return n;

    return f(n/2) + 1;
}

int g(int n) {
    double k = (double) n;

    return 2*(floor(log2(k))+1);
}

int h(int n) { /* Par */
    double k = (double) n;

    return  2*(floor(log2(k))) + 1;
}

int j(int n) { /* Impar */
    double k = (double) n;

    return  2*(floor(log2(k-1))) + 1;
}

int main() {
    int aux;

    for (int i = 0; ; ++i) {
        aux = (i % 2) == 0 ? h(i) : j(i);
        printf("f(%2d) | %2d | %2d | %2d\n", i, f(i), g(i), aux);
    }
}
