#include <stdio.h>
#include <math.h>

/* Parte Esquerda do Predicado */
/* Retorna o coeficiente binomial - n escolhe k */
int coeficienteBinomial(int n, int k){
    if (k == 0) return 1;

    if (1 <= k && k <= n) 
        return coeficienteBinomial(n-1, k) + coeficienteBinomial(n-1, k-1);

    return 0;
}

/* Parte Direita do Predicado */
/* Retorna 2^n */
//double exp2(double x);

int main(){
    double exp;
    int n, k, sum; 
        
    k = 0; sum = 0;
    printf("Insira n: "); scanf("%d", &n);

    for (k = 0; k <= n; k++)
        sum += coeficienteBinomial(n, k);

    exp = exp2(n);
    
    printf("Sigma_{i=0}^{n}:%d | 2^{n}:%.0lf\n", sum, exp);

    return 0;
}

