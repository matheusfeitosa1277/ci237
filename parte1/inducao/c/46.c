#include <stdio.h>
#include <math.h>

/* Parte Esquerda do Predicado */
int summation(int n){
    int i, sum = 0;

    for (i = 0; i <= n; i++){
        sum += i*exp2(i);
    }

    return sum;
}

/* Parte Direita do Predicado */
int expr(int n){
    return exp2(n+1)*(n-1)+2;
}

int main(){
    unsigned long long int n, sum, exp;
    
    printf("Digite n: "); scanf("%d", &n);

    sum = summation(n);
    exp = expr(n);

    printf("Sum:%d Exp:%d\n", sum, exp);

    return 0;
}

